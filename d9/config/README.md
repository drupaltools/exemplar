# Exemplar

Serves as a model or example.

Inject configuration values into services in Drupal 8

https://www.codimth.com/blog/inject-configuration-values-services-drupal-8

Create mail_module.services.yml file:

```yml
services:
  app.mailer:
    class:       Drupal/mail_module/Mailer
    factory:      Drupal/mail_module/MailerFactory:create
    arguments:    ['@config.factory']
```


Create MailerFactory.php file:
```php
class MailerFactory {
    static function create($config) {
        return new Mailer($config->get('mail.config')->get('transport'));
    }
}
```

Create MailerFactory.php file:
```php
class Mailer {
    public function __construct($transport) {
        $this->mailTransport = $transport;
    }
}
```